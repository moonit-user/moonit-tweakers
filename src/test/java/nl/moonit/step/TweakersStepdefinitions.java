package nl.moonit.step;


import io.cucumber.java.en.Given;
import nl.moonit.page.TweakersPage;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;

public class TweakersStepdefinitions {

    private String urlTweakers;
    private TweakersPage tweakersPage;
    private WebDriver webDriver;

    public TweakersStepdefinitions(TweakersPage tweakersPage,
                                   @Value("${urlTweakers}") String urlTweakers,
                                   WebDriver webDriver) {
        this.urlTweakers = urlTweakers;
        this.tweakersPage = tweakersPage;
        this.webDriver = webDriver;
    }

    @Given("^navigate to Tweakers page$")
    public void navigateToTweakersPage() {
        webDriver.get(urlTweakers);
    }

}
